package spring.crud.web;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import spring.crud.service.User;
import spring.crud.service.UserService;

@Controller
public class InputController {
	@Autowired
	private UserService userService;

	@RequestMapping("/showForm")
	public String showInputForm() {
		return "adduserform";
	}

	@RequestMapping("/saveForm")
	public String saveForm(HttpServletRequest request, Model model) {
		String userName = request.getParameter("userName");
		String userPassword = request.getParameter("userPassword");
		String userEmail = request.getParameter("userEmail");
		String userGender = request.getParameter("userGender");
		String userCountry = request.getParameter("userCountry");

		userService.insertUser(userName, userPassword, userEmail, userGender, userCountry);
		model.addAttribute("userList", userService.listAll());

		return "adduser-success";
	}

	@RequestMapping("/showAllUsers")
	public String showUsers(Model model) {
		model.addAttribute("userList", userService.listAll());

		return "displayusers";
	}

	@RequestMapping("/deleteUser")
	public String deleteUser(HttpServletRequest request, Model model) {
		int userId = Integer.valueOf((String) request.getParameter("userId"));
		userService.deleteUser(userId);
		model.addAttribute("userList", userService.listAll());
		return "displayusers";
	}

	@RequestMapping("/editUser")
	public String editUser(HttpServletRequest request, Model model) {
		int userId = Integer.valueOf((String) request.getParameter("userId"));

		model.addAttribute("user", userService.selectById(userId));

		return "edituser";

	}
	
	@RequestMapping("/saveEditedUser")
	public String saveEditedUser(HttpServletRequest request,Model model) {
		int userId = Integer.valueOf((String) request.getParameter("userId"));
		String userName = request.getParameter("userName");
		String userPassword = request.getParameter("userPassword");
		String userEmail = request.getParameter("userEmail");
		String userGender = request.getParameter("userGender");
		String userCountry = request.getParameter("userCountry");
		userService.updateUser(userId, userName, userPassword, userEmail, userGender, userCountry);
		model.addAttribute("userList", userService.listAll());
		return "displayusers";
	}
}
