package spring.crud.service;

import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends GenericDaoImpl<User> implements GenericDao<User>{

//	private Map<Integer, User> userMap = new HashMap<>();// if its not initialized the value is null and u will throw
//															// and exception
//
//	private Integer userId = 0;// start from 0 so the first person will have an index of 1
//
//	public void deleteUser(int userId) {
//		userMap.remove(userId);
//	}
//
//	public Map<Integer, User> getUserMap() {
//		return userMap;
//	}
//
//	public void insertUser(String name, String password, String email, String gender, String country) {
//		userId++;
//		User user = new User();
//		populateUser(name, password, email, gender, country, user, userId);
//		userMap.put(userId, user);
//
//	}
//
//	public Collection<User> listAll() {
//		return userMap.values();
//	}
//
//	public void populateUser(String name, String password, String email, String gender, String country, User user,
//			int userId) {
//		user.setName(name);
//		user.setPassword(password);
//		user.setEmail(email);
//		user.setGender(gender);
//		user.setCountry(country);
//		user.setUserId(userId);
//	}
//
//	@Override
//	public User selectById(int userId) {
//		User user = userMap.get(userId);
//		return user;
//	}
//
//	public void setUserMap(Map<Integer, User> userMap) {
//		this.userMap = userMap;
//	}
//
//	public void updateUser(int userId, String name, String password, String email, String gender, String country) {
//		User user = userMap.get(userId);
//		populateUser(name, password, email, gender, country, user, userId);
//		userMap.put(userId, user);
//	}

}
