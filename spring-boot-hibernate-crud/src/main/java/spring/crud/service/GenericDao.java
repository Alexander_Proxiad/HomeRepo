package spring.crud.service;

import java.io.Serializable;
import java.util.List;

public interface GenericDao<T> {

Serializable save(final T o);

void delete(final Long id);

T get(final Long id);

void update(final T o);

List<T> getAll();	

}
