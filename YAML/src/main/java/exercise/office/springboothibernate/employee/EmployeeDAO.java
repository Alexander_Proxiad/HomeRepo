package exercise.office.springboothibernate.employee;

import java.util.List;

import exercise.office.springboothibernate.GenericDao;

public interface EmployeeDAO extends GenericDao<Employee> {

	public void createEmployee(Employee employee);
	
	List<Employee> listBy(String firstName);
	
}