package exercise.office.springboothibernate.employee;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "employee")
@Getter
@Setter
@ToString
@NoArgsConstructor
public class Employee {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	@Column(name = "name")
	private String name;
	@Column(name = "date_of_joining")
	private Date doj;
	@Column(name = "authority")
	private String authority;
	@Column(name = "salary")
	private Double salary;

	public Employee(String name, String authority, Double salary) {
		super();
		this.name = name;
		this.authority = authority;
		this.salary = salary;
	}

}
