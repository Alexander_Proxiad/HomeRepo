package exercise.office.springboothibernate.employee;

import java.util.Collection;

public interface EmployeeService {
	
	Collection<Employee> filter();

}
