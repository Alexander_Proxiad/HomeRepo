package exercise.office.springboothibernate.selenium;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class AddSeleniumTest {

	public static void main(String[] args) {
		try {
			WebDriver driver = new ChromeDriver();
			driver.get("http://localhost:8080/view");
			WebElement addButton = driver.findElement(By.xpath("/html/body/input"));
			addButton.click();
			WebElement nameField = driver.findElement(By.xpath("//*[@id=\"name\"]"));
			nameField.sendKeys("Dimitar");
			WebElement dateField = driver.findElement(By.xpath("//*[@id=\"doj\"]"));
			dateField.sendKeys("12/12/12 12:12:12");
			WebElement selectDropMenu = driver.findElement(By.xpath("//*[@id=\"authority\"]"));
			Select select = new Select(selectDropMenu);
			select.selectByVisibleText("Admin");
			WebElement salaryField = driver.findElement(By.xpath("//*[@id=\"salary\"]"));
			salaryField.sendKeys("1250.0");
			Thread.sleep(5000);
			WebElement saveButton = driver.findElement(By.xpath("//*[@id=\"employee\"]/table/tbody/tr[5]/td[2]/input"));
			saveButton.click();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
