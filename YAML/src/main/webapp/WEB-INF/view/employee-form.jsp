<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Form</title>
</head>
<body>

	<form:form action="saveEmployee" modelAttribute="employee"
		method="post">
		<!-- Redirection to the Controller with mapping "saveCustomer" and in the controller
		Accessing the modelAttribute with @ModelAttribute("customer") and binding the filled in user
		to a variable
		the method is post - so in the controller @PostMapping should be used
	 -->
		<form:hidden path="id" />
		<!-- property for data binding in this case is ID when we user UPDATE-->
		<table>
			<tbody>
				<tr>
					<td><label>First name:</label></td>
					<td><form:input path="name" />
				</tr>
				<tr>
					<td><label>Date of join:</label></td>
					<td><form:input path="doj" />
				</tr>
				<tr>
					<td><label>Authority:</label></td>
					<td><form:select path="authority">
						<form:option value="Employee">Employee</form:option>
						<form:option value="Secretary">Secretary</form:option>
						<form:option value="Manager">Manager</form:option>
						<form:option value="Admin">Admin</form:option>
					</form:select>
				</tr>
				<tr>
					<td><label>Salary:</label></td>
					<td><form:input path="salary" />
				</tr>
				<tr>
					<td><label></label></td>
					<td><input type="submit" value="Save" /></td>
				</tr>
			</tbody>
		</table>
	</form:form>

	<p>
		<a href="${pageContext.request.contextPath}/view">Back to
			List</a>
	</p>

</body>
</html>