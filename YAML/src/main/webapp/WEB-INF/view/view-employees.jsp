<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<input type="submit" value="Add Employee"
		onclick="window.location.href='form';return false">
	<!-- when the button is clicked the user is redirected to the controller with the current mapping -->

	<table>
		<tr>
			<th>First Name</th>
			<th>Date of joining</th>
			<th>Authority</th>
			<th>Salary</th>
			<th>Update</th>
			<th>Delete</th>
		</tr>
		<c:forEach var="tempEmployee" items="${employees}">

			<c:url var="updateLink" value="/update">
				<c:param name="employeeId" value="${tempEmployee.id}"></c:param>
			</c:url>
			<!-- sets the url that is going to be passed in the table collums via EL,the link is set to a concrete mapping
				also the parameter is later accessed with the @RequestParameter in the conrtoller in order to get the 
				current Customer via his Unique ID,value is accessed via EL tempCustomer.id
			 -->
			<c:url var="deleteLink" value="/delete">
				<c:param name="employeeId" value="${tempEmployee.id}"></c:param>
			</c:url>

			<tr>
				<td>${tempEmployee.name}</td>
				<td>${tempEmployee.doj}</td>
				<td>${tempEmployee.authority}</td>
				<td>${tempEmployee.salary}</td>
				<td><a href="${updateLink}">Update</a></td><!-- El variable reference used as a link -->
				<td><a href="${deleteLink}">Delete</a></td><!-- El variable reference used as a link -->
			</tr>
		</c:forEach>
	</table>
</body>
</html>