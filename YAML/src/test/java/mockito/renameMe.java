package mockito;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.internal.verification.AtLeast;
import org.mockito.internal.verification.Times;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.verification.VerificationMode;

import exercise.office.springboothibernate.employee.Employee;
import exercise.office.springboothibernate.employee.EmployeeDAO;
import exercise.office.springboothibernate.employee.EmployeeServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class renameMe {
	
	@Mock
	EmployeeDAO edi;
	
	@InjectMocks
	EmployeeServiceImpl employeeService;
	
	Employee tempEmployee = new Employee();
	
//	int employeeId = 1;

	@Test
	public void test() {
		
		List<Employee> allEmployees = new ArrayList<>();
		allEmployees.add(new Employee("Alex", "Employee", 500.0));
		allEmployees.add(new Employee("Svenja", "Admin", 100.0));
		
		when(edi.getAll()).thenReturn(allEmployees);
		Collection<Employee> filtyereddList = employeeService.filter();
		verify(edi, atLeastOnce()).getAll();
		assertEquals(1, filtyereddList.size());
		
	}

}
