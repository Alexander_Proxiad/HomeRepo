package luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import luv2code.hibernate.demo.entity.Course;
import luv2code.hibernate.demo.entity.Instructor;
import luv2code.hibernate.demo.entity.InstructorDetail;
import luv2code.hibernate.demo.entity.Review;
import luv2code.hibernate.demo.entity.Student;

public class AddCoursesForSvenjaDemo {

	public static void main(String[] args) {
		SessionFactory factory = new Configuration()
				.configure()
				.addAnnotatedClass(Instructor.class)
				.addAnnotatedClass(InstructorDetail.class)
				.addAnnotatedClass(Course.class)
				.addAnnotatedClass(Review.class)
				.addAnnotatedClass(Student.class)
				.buildSessionFactory();
		Session session = factory.getCurrentSession();
		
		try {
			
			session.beginTransaction();
			int theId = 2;
			//get Svenja from the db
			Student tempStudent = session.get(Student.class, theId);
			
			System.out.println("\nLoaded student " + tempStudent);
			System.out.println("Course: " + tempStudent.getCourses());
			
			//create more courses
			Course tempCourse1 = new Course("Rubik's cube - how to solve");
			Course tempCourse2 = new Course("Atari 2600 - game dev");
			
			//addstudent to courses
			tempCourse1.addStudent(tempStudent);
			tempCourse2.addStudent(tempStudent);
			//save the courses
			System.out.println("\nSaving the courses");
			
			session.save(tempCourse1);
			session.save(tempCourse2);
			
			session.getTransaction().commit();
			System.out.println("DONE");
			
		}finally {
			session.close();
			factory.close();
		}

	}

}
