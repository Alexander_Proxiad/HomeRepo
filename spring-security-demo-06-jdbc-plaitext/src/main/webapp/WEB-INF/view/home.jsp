<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<html>

<head>
<title>luv2code Company Home Page</title>
</head>

<body>
	<h2>luv2code Company Home Page- Yahooo-Pidar Blyat</h2>
	<hr>

	Welcome to the luv2code company home page!

	<hr>

	<p>
		User:
		<security:authentication property="principal.username" />
		<br /> Role:
		<security:authentication property="principal.authorities" />
	</p>

	
	<security:authorize access="hasRole('MANAGER')">
	<hr>
		<a href="${pageContext.request.contextPath}/leaders">LeaderShip
			Meeting</a>
		(Only for Manager peeps)
	</p>
	</security:authorize>
	<p>
	

	<security:authorize access="hasRole('ADMIN')">
	<hr>
		<p>
			<a href="${pageContext.request.contextPath}/systems">IT ADMIN
				meating</a> (Only for ADMINS)
		</p>
	</security:authorize>
	<hr>


	<form:form action="${pageContext.request.contextPath}/logout"
		method="POST">

		<input type="submit" value="Logout" />

	</form:form>

</body>

</html>