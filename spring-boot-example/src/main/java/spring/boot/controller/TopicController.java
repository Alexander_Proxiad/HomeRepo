package spring.boot.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import spring.boot.Topic;
import spring.boot.service.TopServiceImpl;

@RestController
public class TopicController {
	@Autowired
	private TopServiceImpl topServiceImpl;
	
	@RequestMapping("/topic")
	public List<Topic> displayTopics(){
		
		return topServiceImpl.getAllTopics();
	}
	@RequestMapping("/topic/{id}")
	public Topic getTopic(@PathVariable String id) {
		return topServiceImpl.getTopic(id);
	}

}
