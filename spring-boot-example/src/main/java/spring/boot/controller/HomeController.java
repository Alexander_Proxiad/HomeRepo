package spring.boot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import spring.boot.service.MathService;
@RestController
public class HomeController {
	
	@Autowired
	private MathService mathService;
	
	@RequestMapping("/")
	public double home() {
		return mathService.addTwoNumbers(5.0, 5.0);
	}

}
