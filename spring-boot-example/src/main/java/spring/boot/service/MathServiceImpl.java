package spring.boot.service;

import org.springframework.stereotype.Service;

@Service
public class MathServiceImpl implements MathService {

	@Override
	public double addTwoNumbers(double a, double b) {
		return a + b;
	}

	

}
