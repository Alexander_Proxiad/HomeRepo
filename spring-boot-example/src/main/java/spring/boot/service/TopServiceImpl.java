package spring.boot.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import spring.boot.Topic;

@Service
public class TopServiceImpl {

	public List<Topic> topics = new ArrayList<>(Arrays.asList(
			new Topic("spring", "boot", "No good tutorials"),
			new Topic("League", "cancer", "Worst game ever"),
			new Topic("Tantra", "booty", "Could use some practice")
			));

	public List<Topic> getAllTopics() {
		return topics;
	}
	
	public Topic getTopic(String id) {
		return topics.stream().filter(t -> t.getId().equals(id)).findFirst().get();
	}

}
