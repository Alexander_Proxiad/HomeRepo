package spring.boot.service;

public interface MathService {
	
	public double addTwoNumbers(double a, double b);

}
