package spring.boot;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class App {
	
	
	
	public static void main(String[]args) {
		
		SpringApplication springApp = new SpringApplication(App.class);
		springApp.setBannerMode(Banner.Mode.OFF);
		springApp.run(args);
		
	}

}
