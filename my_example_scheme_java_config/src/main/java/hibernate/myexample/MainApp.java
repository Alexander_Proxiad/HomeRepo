package hibernate.myexample;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import hibernate.myexample.entity.Person;
import hibernate.myexample.util.HibernateUtil;

public class MainApp {

	public static void main(String[] args) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		
		Session session = sessionFactory.openSession();
		
		try {
			
			session.beginTransaction();
			
			Person person = new Person();
			person.setName("White Rabbit");
			session.save(person);
			
			Person person1 = session.get(Person.class, 1L);
			System.out.println(person1.getName());
			
			session.getTransaction().commit();
			
		}finally {
			session.close();
			HibernateUtil.shutdown();
		}

	}

}
