package many.to.many.hibernate.config;

import java.util.Properties;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import many.to.many.hibernate.entity.Course;
import many.to.many.hibernate.entity.Student;

public class HibernateUtil {

	private static final SessionFactory sessionFactory = buildSessionFactory();

	private static SessionFactory buildSessionFactory() {
		try {
			return new Configuration().addProperties(createProperties()).addAnnotatedClass(Student.class)
					.addAnnotatedClass(Course.class).buildSessionFactory();
		} catch (Throwable ex) {
			throw new ExceptionInInitializerError(ex);
		}

	}

	private static Properties createProperties() {
		Properties properties = new Properties();
		properties.setProperty("hibernate.connection.driver_class", "com.mysql.cj.jdbc.Driver");
		properties.setProperty("hibernate.connection.url", "jdbc:mysql://localhost:3306/my-many-to-many-example?useSSL=false");
		properties.setProperty("hibernate.connection.username", "root");
		properties.setProperty("hibernate.connection.password", "19941905a");
		properties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
		properties.setProperty("hibernate.show_sql", "true");
//		properties.setProperty("hibernate.hbm2ddl.auto", "create");
		return properties;
	}

	public static void shutdown() {
		getSessionFactory().close();
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

}
