package many.to.many.hibernate.application;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import many.to.many.hibernate.config.HibernateUtil;
import many.to.many.hibernate.entity.Course;
import many.to.many.hibernate.entity.Student;

public class CreateMain {

	public static void main(String[] args) {
		
		SessionFactory factory = HibernateUtil.getSessionFactory();
		
		Session session = factory.openSession();
		
		try {
			//begin the transaction
			session.beginTransaction();
			//create student
			Student theStudent = new Student("Alex", "Vulchev", "Alex@mail.bg");
			session.save(theStudent);
			//create Course
			Course theCourse = new Course("Learn Hibernate");
			Course theCourse1 = new Course("How to create games in Java");
			//Add the course to the student
			theStudent.addCourse(theCourse);
			theStudent.addCourse(theCourse1);
			//save the 
			session.save(theCourse);
			session.save(theCourse1);
			//commit the transaction
			session.getTransaction().commit();
		}finally {
			HibernateUtil.shutdown();
			session.close();
		}

	}

}
