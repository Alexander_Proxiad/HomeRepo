package many.to.many.hibernate.application;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import many.to.many.hibernate.config.HibernateUtil;
import many.to.many.hibernate.entity.Student;

public class DisplayMain {
	
	public static void main(String[]args) {
		
		SessionFactory factory = HibernateUtil.getSessionFactory();
		
		Session session = factory.openSession();
		
		try {
			session.beginTransaction();
			int theId = 5;
			Student tempStudent = session.get(Student.class, theId);
			
			System.out.println("\nLoaded student " + tempStudent);
			System.out.println("Course: " + tempStudent.getCourses());
			
//			List<Student> students = session.createQuery("from Student").getResultList();
//			System.out.print(students);
			
			session.getTransaction().commit();
			System.out.println("DONE");
		}finally {
			HibernateUtil.shutdown();
			session.close();
		}
		
	}

}
