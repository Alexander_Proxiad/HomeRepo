package many.to.many.hibernate.application;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import many.to.many.hibernate.config.HibernateUtil;
import many.to.many.hibernate.entity.Student;

public class UpdateMain {

	public static void main(String[] args) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		
		Session session = factory.openSession();
		
		try {
			
			session.beginTransaction();
			int studentId = 5;
			
			Student tempStudent = session.get(Student.class, studentId);
			tempStudent.setFirstName("Kamen");
			session.saveOrUpdate(tempStudent);
			
			session.getTransaction().commit();
			
		}finally {
			HibernateUtil.shutdown();
			session.close();
		}
	}

}
