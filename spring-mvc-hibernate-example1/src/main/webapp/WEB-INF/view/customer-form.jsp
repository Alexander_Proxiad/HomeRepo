<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<form:form action="saveCustomer" modelAttribute="customer" method="post">
	<!-- Redirection to the Controller with mapping "saveCustomer" and in the controller
		Accessing the modelAttribute with @ModelAttribute("customer") and binding the filled in user
		to a variable
		the method is post - so in the controller @PostMapping should be used
	 -->
	<form:hidden path="id"/><!-- property for data binding in this case is ID when we user UPDATE-->
		<table>
			<tbody>
				<tr>
					<td><label>First name:</label></td>
					<td><form:input path="firstName"/>
				</tr>
				<tr>
					<td><label>Last name:</label></td>
					<td><form:input path="lastName"/>
				</tr>
				<tr>
					<td><label>Email:</label></td>
					<td><form:input path="email"/>
				</tr>
				<tr>
					<td><label></label></td>
					<td><input type="submit" value="Save"/></td>
				</tr>
			</tbody>
		</table>
	</form:form>
	
	<p>
		<a href="${pageContext.request.contextPath}/customer/view">Back to List</a>
		<!-- requests contextPath in order to get the mapping of the controller that is wanted -->
	</p>

</body>
</html>