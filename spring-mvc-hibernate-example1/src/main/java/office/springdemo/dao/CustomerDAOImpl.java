package office.springdemo.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import office.springdemo.entity.Customer;
@Repository //Annotates a class as repo - encapsulating storage and logic - The @Repository annotation makes the class a bean 
public class CustomerDAOImpl implements CustomerDAO {
	
	@Autowired//Autowiring the SessionFactory that is specified in the XML configuration files
	private SessionFactory sessionFactory;
	
	@Override
	public List<Customer> getCustomers() {//Method for listing the customers
		
		Session session = sessionFactory.getCurrentSession();
		
		Query<Customer> theQuery = session.createQuery("from Customer order by firstName", Customer.class);
		
		List<Customer> customers = theQuery.getResultList();
		
		return customers;
	}

	@Override
	public void saveCustomer(Customer theCustomer) {//Method for saving the customers
		Session session = sessionFactory.getCurrentSession();
		
		session.saveOrUpdate(theCustomer);// if the Customer does not exist Save, if he exists the Update
	}

	@Override
	public Customer getCustomer(int theId) {
		Session session = sessionFactory.getCurrentSession();
		
		Customer theCustomer = session.get(Customer.class, theId);//return the customer via ID
		return theCustomer;
	}

	@Override
	public void deleteCustomer(int theId) {
		
		Session session = sessionFactory.getCurrentSession();
		
		Customer theCustomer = session.get(Customer.class, theId);//retrive the customer by ID so he can be deleted
		session.delete(theCustomer);
	}

	


}
