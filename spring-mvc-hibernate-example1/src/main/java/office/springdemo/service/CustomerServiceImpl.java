package office.springdemo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import office.springdemo.dao.CustomerDAO;
import office.springdemo.entity.Customer;
@Service //indicates the class is a service - Has the logic from the DAO -@Service annotation makes this class a bean
public class CustomerServiceImpl implements CustomerService{

	@Autowired //Autowiring the logic that is written in the DAO class
	private CustomerDAO customerDAO;
	
	@Override
	@Transactional //Performs the session.beginTransaction() and session.getTransaction().commit() methods
	public List<Customer> getCustomers() {
		return customerDAO.getCustomers();//gets the method from the DAO
	}

	@Override
	@Transactional
	public void saveCustomer(Customer theCustomer) {
		customerDAO.saveCustomer(theCustomer);
		
	}

	@Override
	@Transactional
	public Customer getCustomer(int theId) {
		return customerDAO.getCustomer(theId);
	}

	@Override
	@Transactional
	public void deleteCustomer(int theId) {
		customerDAO.deleteCustomer(theId);
		
	}

	
	
}
