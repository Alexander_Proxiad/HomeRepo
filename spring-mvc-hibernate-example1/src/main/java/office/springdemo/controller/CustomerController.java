package office.springdemo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import office.springdemo.entity.Customer;
import office.springdemo.service.CustomerService;

@Controller //every controller must be annotated as one
@RequestMapping("/customer") //request mapping for this current controller class
public class CustomerController {
	
	@Autowired // Autowiring the CustomerService interface that contains the DAO 
	private CustomerService CustomerService;
	
	@GetMapping("/view") //Annotating with getMapping - handles get Requests
	public String listCustomers(Model theModel) {
		
		List<Customer> theCustomers = CustomerService.getCustomers();//Extracting the customers in the database and storing them in a list
		
		theModel.addAttribute("customers", theCustomers);//saving the List in the model under the name of "customers" 
		
		return "view-customers";// returns /WEB-INF/view-customers.jsp
	}
	
	@GetMapping("/showFormForAdd")// Annotating with getMapping - handles getRequests
	public String showFormForAdd(Model theModel) {
		
		Customer theCustomer = new Customer();//empty user that will be populated in the form
		
		theModel.addAttribute("customer", theCustomer);//saving the List in the model under the name of "customers" 
		return "customer-form";// returns /WEB-INF/customer-form.jsp
		
	}
	
	@PostMapping("saveCustomer")//Annotating with postMapping -handles post requests
	public String saveCustomer(@ModelAttribute("customer")Customer theCustomer) { //@ModelAttribute binds "customer" to the variable theCustomer
		
		CustomerService.saveCustomer(theCustomer);//Saving to the database through customerService and theCustomer that is passed in a attribute of customer
		
		return "redirect:/customer/view";//using redirect to go to a controller!!!!NOTE the value passed is a MAPPING not JSP
		
	}
	
	@GetMapping("/showFormForUpdate")//Annotating with getMapping-getRequests
	public String showFormForUpdate(@RequestParam("customerId")int theId,Model theModel) {//@RequestParam- the parameter "customerId" is requested from the jsp and then bound to "theId"
		
		Customer theCustomer = CustomerService.getCustomer(theId);//Extracting the Customer with the given id
		
		theModel.addAttribute("customer", theCustomer);//Saving theCustomer under "customer"
		
		return "customer-form";//returns /WEB-INF/customer-form.jsp
		
		
	}
	
	@GetMapping("/delete")
	public String deleteCustomer(@RequestParam("customerId")int theId) {//@RequestParam- the parameter "customerId" is requested from the jsp and then bound to "theId"
		CustomerService.deleteCustomer(theId);//deleting a person via a unique key
		return "redirect:/customer/view";//using redirect to go to a controller!!!!NOTE the value passed is a MAPPING not JSP
	}
	
}
