package luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import luv2code.hibernate.demo.entity.Instructor;
import luv2code.hibernate.demo.entity.InstructorDetail;

public class CreateInstructorDemo {

	public static void main(String[] args) {
		SessionFactory factory = new Configuration()
				.configure()
				.addAnnotatedClass(Instructor.class)
				.addAnnotatedClass(InstructorDetail.class)
				.buildSessionFactory();
		Session session = factory.getCurrentSession();
		
		try {
			
			session.beginTransaction();
			Instructor tempInstructor = new Instructor("Svenja", "Ruprecht", "sleeping@de.de");
			InstructorDetail tempIDetail = new InstructorDetail("BeautyCV", "Alex");
			tempInstructor.setInstructorDetail(tempIDetail);
			
			session.save(tempInstructor);
			
			session.getTransaction().commit();
			System.out.println("Your create operation has executed successfully!!");
			
		}finally {
			session.close();
			factory.close();
		}

	}

}
