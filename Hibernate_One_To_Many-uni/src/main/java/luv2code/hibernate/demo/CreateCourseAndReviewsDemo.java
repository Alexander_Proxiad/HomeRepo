package luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import luv2code.hibernate.demo.config.HibernateUtil;
import luv2code.hibernate.demo.entity.Course;
import luv2code.hibernate.demo.entity.Instructor;
import luv2code.hibernate.demo.entity.InstructorDetail;
import luv2code.hibernate.demo.entity.Review;

public class CreateCourseAndReviewsDemo {

	public static void main(String[] args) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		
		try {
			
			session.beginTransaction();
			
		
			//create a course
			Course tempCourse = new Course("plsplspls");
			//add some reviews
			tempCourse.addReview(new Review("aaa"));
			tempCourse.addReview(new Review("bbbbb"));
			tempCourse.addReview(new Review("Pvv"));
			//save the course.. and leverage the cascade all
			System.out.println("Saving the course");
			System.out.println(tempCourse);
			System.out.println(tempCourse.getReviews());
			session.save(tempCourse);
			session.getTransaction().commit();
			System.out.println("DONE");
			
		}finally {
			session.close();
			HibernateUtil.shutdown();
		}

	}

}
