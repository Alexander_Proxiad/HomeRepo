package exercise.office.springboothibernate;

import exercise.office.springboothibernate.entities.Employee;

public interface EmployeeDAO extends GenericDao<Employee> {

	public void createEmployee(Employee employee);
	
}