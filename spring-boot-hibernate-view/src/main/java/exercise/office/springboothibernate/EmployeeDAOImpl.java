package exercise.office.springboothibernate;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import exercise.office.springboothibernate.entities.Employee;

@Repository
@Transactional
public class EmployeeDAOImpl extends GenericDaoImpl<Employee> implements EmployeeDAO {
	
	public void createEmployee(Employee employee) {		
		super.save(employee);
	}
}
