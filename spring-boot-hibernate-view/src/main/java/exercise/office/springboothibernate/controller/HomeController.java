package exercise.office.springboothibernate.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import exercise.office.springboothibernate.EmployeeDAO;

@Controller
public class HomeController {
	
	@Autowired
	private EmployeeDAO employeeDao;
	
	@RequestMapping("/")
	public String displayEmployeeForm() {
		
		
		
		return "employee-form";
	}
	

}
