package exercise.office.springboothibernate.selenium;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class UpdateSeleniumTest {

	public static void main(String[] args) {
		try {
		
			WebDriver driver = new ChromeDriver();
			driver.get("http://localhost:8080/view");
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			WebElement updateLink = driver.findElement(By.xpath("/html/body/table/tbody/tr[2]/td[5]/a"));
			updateLink.click();
			WebElement firstNameField = driver.findElement(By.xpath("//*[@id=\"name\"]"));
			firstNameField.clear();
			WebElement dateField = driver.findElement(By.xpath("//*[@id=\"doj\"]"));
			dateField.clear();
			WebElement selectDropMenu = driver.findElement(By.xpath("//*[@id=\"authority\"]"));
			WebElement salaryField = driver.findElement(By.xpath("//*[@id=\"salary\"]"));
			salaryField.clear();
			Thread.sleep(5000);
			firstNameField.sendKeys("Editocho");
			dateField.sendKeys("2018/09/18 14:54:1");
			Select select = new Select(selectDropMenu);
			select.selectByVisibleText("Manager");
			salaryField.sendKeys("1234.5");
			WebElement saveButton = driver.findElement(By.xpath("//*[@id=\"employee\"]/table/tbody/tr[5]/td[2]/input"));
			saveButton.click();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
