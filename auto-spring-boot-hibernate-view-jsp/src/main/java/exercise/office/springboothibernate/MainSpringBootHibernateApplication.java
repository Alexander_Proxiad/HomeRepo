package exercise.office.springboothibernate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
@EnableJpaAuditing
public class MainSpringBootHibernateApplication {

	public static void main(String[] args) {
		SpringApplication.run(MainSpringBootHibernateApplication.class, args);
	}
}
