package exercise.office.springboothibernate;
import java.io.Serializable;
import java.util.List;

public interface GenericDao<T> {

Serializable save(final T o);

void delete(final int id);

T get(final int id);

void update(final T o);

List<T> getAll();	

}