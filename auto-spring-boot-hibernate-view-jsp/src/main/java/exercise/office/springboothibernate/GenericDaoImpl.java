package exercise.office.springboothibernate;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.hibernate.Session;

@Transactional
public class GenericDaoImpl<T> implements GenericDao<T> {

	protected Class<T> clazz;

	@PersistenceContext
	protected EntityManager em;

	@SuppressWarnings("deprecation")
	protected Session getSession() {
		return em.unwrap(Session.class).getSession();
	}

	@SuppressWarnings("unchecked")
	public GenericDaoImpl() {
		ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
		this.clazz = (Class<T>) genericSuperclass.getActualTypeArguments()[0];
	}

	@Override
	public Serializable save(T o) {
		return getSession().save(o);
	}

	@Override
	public void delete(int id) {
		T objToDelete = getSession().load(clazz, id);
		getSession().delete(objToDelete);
	}

	@Override
	public T get(int id) {
		return getSession().load(clazz, id);
	}

	@Override
	public void update(T o) {
		getSession().saveOrUpdate(o);
	}

	@Override
	public List<T> getAll() {
		CriteriaBuilder builder = getSession().getCriteriaBuilder();
		CriteriaQuery<T> query = builder.createQuery(clazz);
		Root<T> from = query.from(clazz);
		query.select(from);

		TypedQuery<T> tq = getSession().createQuery(query);
		return tq.getResultList();
	}
}
