package com.luv2code.springsecurity.demo.config;


import java.beans.PropertyVetoException;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.mchange.v2.c3p0.ComboPooledDataSource;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages="com.luv2code.springsecurity.demo")
@PropertySource("classpath:persistence-mysql.properties")
public class DemoAppConfig {
	//holds the data from the properties
	@Autowired
	private Environment env;
	
	//diagnostics
	
	private Logger logger = Logger.getLogger(getClass().getName());

	// define a bean for ViewResolver

	@Bean
	public ViewResolver viewResolver() {
		
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		
		viewResolver.setPrefix("/WEB-INF/view/");
		viewResolver.setSuffix(".jsp");
		
		return viewResolver;
	}
	
	//define a bean for our security datasource
	
	@Bean
	public DataSource seacurityDataSource() {
		
		//create a connection pool
		ComboPooledDataSource securityDataSouce
									= new ComboPooledDataSource();
		//set the jdbc driver class
		try {
			securityDataSouce.setDriverClass(env.getProperty("jdbc.driver"));
		} catch (PropertyVetoException e) {
			throw new RuntimeException(e);
		}
		//log the connection properties
		logger.info(">>> jdbc.url= " + env.getProperty("jdbc.url"));
		logger.info(">>> jdbc.user= " + env.getProperty("jdbc.user"));
		//set database connection props
		
		securityDataSouce.setJdbcUrl(env.getProperty("jdbc.url"));
		securityDataSouce.setUser(env.getProperty("jdbc.user"));
		securityDataSouce.setPassword(env.getProperty("jdbc.password"));
		//set connection pool props
		
		securityDataSouce.setInitialPoolSize(getIntProperty("connection.pool.initialPoolSize"));
		securityDataSouce.setMinPoolSize(getIntProperty("connection.pool.minPoolSize"));
		securityDataSouce.setMaxPoolSize(getIntProperty("connection.pool.maxPoolSize"));
		securityDataSouce.setMaxIdleTime(getIntProperty("connection.pool.maxIdleTime"));
		
		
		return securityDataSouce;
	}
	
	//helper method
	private int getIntProperty(String propName) {
		String propVal = env.getProperty(propName);
		
		//now convert to int
		
		int intPropVal = Integer.parseInt(propVal);
		
		return intPropVal;
	}
	
}









