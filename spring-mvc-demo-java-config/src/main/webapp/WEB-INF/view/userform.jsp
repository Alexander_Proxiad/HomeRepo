<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add new user</title>
</head>
<body>
	<a href="showAllUsers">View All Records</a>
	<h1>Add New User</h1>
	<form action="saveForm" method="post">
		<table border="1">
			<!--  <tr>
				<td>ID:</td>
				<td><input type="text" name="userId"/></td>
			</tr>-->
			<tr>
				<td>Name:</td>
				<td><input type="text" name="userName"/></td>
			</tr>
			<tr>
				<td>Password:</td>
				<td><input type="password" name="userPassword"/></td>
			</tr>
			<tr>
				<td>Email:</td>
				<td><input type="text" name="userEmail"/></td>
			</tr>
			<tr>
				<td>Gender:</td>
				<td>
					<input type="radio" name="userGender" value="male" checked/>Male
					<input type="radio" name="userGender" value="female"/>Female
				</td>
			</tr>
			<tr>
				<td>Country:</td>
				<td>
					<select name="userCountry">
						<option>Bulgaria</option>
						<option>Turkey</option>
						<option>Greece</option>
						<option>Romania</option>
					</select>
				</td>
			</tr>
			<tr>
			<td><input type="submit" value="Add User"/></td>
			<td><input type="reset" value="Reset"/></td>
			</tr>
		</table>
	</form>
</body>
</html>
