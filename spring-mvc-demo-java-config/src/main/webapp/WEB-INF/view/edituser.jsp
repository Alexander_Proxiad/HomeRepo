<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Edit User</title>
</head>
<body>
	<form action="saveEditedUser?userId=${user.userId}" method="post">
		<input type="hidden" name="userId" value="${user.getUserId()}"/>  
		<table border="1">
		<tr>
				<td>Name:</td>
				<td><input type="text" name="userName" value="${user.getName()}"/></td>
			</tr>
			<tr>
				<td>Password:</td>
				<td><input type="password" name="userPassword" value="${user.getPassword()}"/></td>
			</tr>
			<tr>
				<td>Email:</td>
				<td><input type="text" name="userEmail" value="${user.getEmail()}"/></td>
			</tr>
			<tr>
				<td>Gender:</td>
				<td>
					<input type="radio" name="userGender" value="male" checked/>Male
					<input type="radio" name="userGender" value="female"/>Female
				</td>
			</tr>
			<tr>
				<td>Country:</td>
				<td>
					<select name="userCountry">
						<option>Bulgaria</option>
						<option>Turkey</option>
						<option>Greece</option>
						<option>Romania</option>
					</select>
				</td>
			</tr>
			<tr><td><input type="submit" value="edit"/></td></tr>
		</table>
	</form>
</body>
</html>
