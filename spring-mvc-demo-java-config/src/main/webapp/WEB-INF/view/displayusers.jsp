<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.util.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>View Users</title>
</head>
<body>
	<table border="1">
		<tr><th>ID</th><th>Name</th><th>Password</th><th>Email</th><th>Gender</th><th>Country</th><th>Edit</th><th>Delete</th></tr>
		<c:forEach items="${userList}" var="user">
			<tr>
			<td>${user.userId}</td>
			<td>${user.name}</td>
			<td>${user.password}</td>
			<td>${user.email}</td>
			<td>${user.gender}</td>
			<td>${user.country}</td>
			<td><a href="editUser?userId=${user.userId}">Edit</a></td>
			<td><a href="deleteUser?userId=${user.userId}">Delete</a></td>
			</tr>		
		</c:forEach>
	</table>
	<br/><a href="adduserform.jsp"></a>
	<br/><a href="">Go to HomePage</a>
</body>
</html>
