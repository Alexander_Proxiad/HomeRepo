package spring.crud.entity;

public class User {

	private String name;
	private String password;
	private String email;
	private String gender;
	private String country;
	private int userId;

	public User() {

	}

	public User(String name, String password, String email, String gender, String country, int userId) {
		super();
		this.name = name;
		this.password = password;
		this.email = email;
		this.gender = gender;
		this.country = country;
		this.userId = userId;
	}



	@Override
	public String toString() {
		return "User [name=" + name + ", password=" + password + ", email=" + email + ", gender=" + gender
				+ ", country=" + country + "]";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;

	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}
