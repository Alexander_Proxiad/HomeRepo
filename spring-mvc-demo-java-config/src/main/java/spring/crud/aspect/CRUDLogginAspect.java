package spring.crud.aspect;

import java.util.logging.Logger;

import org.aopalliance.intercept.Joinpoint;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class CRUDLogginAspect {
	
	private Logger myLogger = Logger.getLogger(getClass().getName());
	
	@Pointcut("execution(* spring.crud.app.*.*(..))")
	private void forControllerPackage() {
		
	}
	
	@Pointcut("execution(* spring.crud.service.*.*(..))")
	private void forServicePackage() {
		
	}
	
	@Pointcut("forControllerPackage() || forServicePackage()")
	private void forAppFlow() {
		
	}
	
	@Before("forAppFlow()")
	public void before(JoinPoint theJointPoint) {
		//display method we are calling
		String theMethod = theJointPoint.getSignature().toShortString();
		myLogger.info("=====>> in @Before: calling method: "  + theMethod);
		
		//display the arguments to the method
		
		//get args
		Object[] args = theJointPoint.getArgs();
		for(Object tempArg : args) {
			myLogger.info("=====>> argument: " + tempArg);
		}
	}
	
	@AfterReturning(
			pointcut="forAppFlow()",
			returning="theResult"
			)
	public void afterReturning(JoinPoint theJoinPoint,Object theResult) {
		//display method we are returning from
				String theMethod = theJoinPoint.getSignature().toShortString();
				myLogger.info("=====>> in @AfterReturning: from method: "  + theMethod);
				
		//display the data returned
				myLogger.info("=====>> result: " + theResult);
	}
}
