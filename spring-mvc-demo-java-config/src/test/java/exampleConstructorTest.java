import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;

import spring.crud.entity.User;

public class exampleConstructorTest {
	
	User user;
	
	@Before
	public void initParams() {
		user = new User("Frodo", "myPrecious", "theRingIsMine@samhero.com", "Male", "Shire",1);
	}

	@Test
	public void test() {
		
		assertThat(user).isNotNull();
		assertThat(user.getName()).isEqualTo("Frodo");
		assertThat(user.getCountry()).isNotEqualTo("Mordor");
		assertThat(user).isInstanceOf(User.class);
		assertThat(user).hasNoNullFieldsOrProperties();
		
	}

}
