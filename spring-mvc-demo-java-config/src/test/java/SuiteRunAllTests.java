import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
	deleteUserTest.class,
	InsertUserTest.class,
	updateUserTest.class,
	listAllTest.class,
	exampleConstructorTest.class
})
public class SuiteRunAllTests {

}
