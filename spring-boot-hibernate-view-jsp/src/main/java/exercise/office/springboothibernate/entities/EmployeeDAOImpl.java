package exercise.office.springboothibernate.entities;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import exercise.office.springboothibernate.GenericDaoImpl;

@Repository
//@Transactional
public class EmployeeDAOImpl extends GenericDaoImpl<Employee> implements EmployeeDAO {
	
	public void createEmployee(Employee employee) {		
		super.save(employee);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Employee> listBy(String name) {
		return getSession().createCriteria(Employee.class)
		.add(Restrictions.ilike("name", name))
		.list();
	}
	
}
