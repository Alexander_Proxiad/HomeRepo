package exercise.office.springboothibernate.entities;

import java.util.Collection;

public interface EmployeeService {
	
	Collection<Employee> filter();

}
