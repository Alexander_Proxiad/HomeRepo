package exercise.office.springboothibernate.entities;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeDAO employeeDAO;
	
	@Override
	public Collection<Employee> filter() {
		Collection<Employee> filteredList = new ArrayList<Employee>();
		Collection<Employee> list = employeeDAO.getAll();
		for (Employee employee : list) {
			if (employee.getName().startsWith("B")) {
				filteredList.add(employee);
			}
		}
		// TODO Auto-generated method stub
		return filteredList;
	}
	
}
