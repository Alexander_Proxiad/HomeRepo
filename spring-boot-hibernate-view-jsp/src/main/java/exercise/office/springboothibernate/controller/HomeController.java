package exercise.office.springboothibernate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import exercise.office.springboothibernate.entities.Employee;
import exercise.office.springboothibernate.entities.EmployeeDAO;

@Controller
public class HomeController {
	
	@Autowired
	private EmployeeDAO employeeDao;
	
	@GetMapping("/form")
	public String displayEmployeeForm(Model theModel) {
		
		Employee employee = new Employee();
		theModel.addAttribute("employee", employee);
		return "employee-form";
	}

	
	@PostMapping("/saveEmployee")
	public String saveEmployee(@ModelAttribute(name="employee")Employee employee) {
		
		employeeDao.update(employee);
		
		return "redirect:/view";
		
	}
	
	@GetMapping("/view")
	public String viewEmployeeList(Model theModel) {
		
		List<Employee> employees = employeeDao.getAll();
		
		theModel.addAttribute("employees", employees);
		
		return "view-employees";
		
	}
	
	@GetMapping("/delete")
	public String deleteEmployee(@RequestParam(name="employeeId")int employeeId) {
		employeeDao.delete(employeeId);
		return "redirect:/view";
	}
	
	@GetMapping("/update")
	public String updateEmployee(@RequestParam(name="employeeId")int employeeId,Model theModel) {
		Employee theEmployee = employeeDao.get(employeeId);
		theModel.addAttribute("employee",theEmployee);
		return "employee-form";
	}
	

}
